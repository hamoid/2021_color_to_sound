#!/bin/sh

nohup scide \
  </dev/null \
  >/tmp/colorToSound_scide.log \
  2>&1 \
  &

sleep 8
cd ws2osc
nohup node \
  </dev/null \
  >/tmp/colorToSound_node.log \
  2>&1 \
  server.js \
  &

sleep 2
nohup firefox \
  </dev/null \
  >/tmp/colorToSound_firefox.log \
  2>&1 \
  "https://localhost:8443#debug" \
  &

