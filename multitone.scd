(
// Time for fx to start going up
~fxInitialSilence = 12; // seconds

// After initial silence, in how many seconds the fx fades in
~fxFadeInDuration = 1;  // seconds

// 1.0 = full grain noise, -1.0 is no grain noise at all
~grainMix = -1.0;

// grain travel speed. maybe reduce if using longer wav files.
~travelSpeed = 0.5;

// to avoid distortion, remove bass
~removeBassBelowFreq = 150.0;

// If all 12 colors are below this saturation % -> silence
// The idea is to mute with monochrome scenes
~saturationMutedBelow = 15;

// Full volume if any of the 12 colors has saturation above this %
~saturationMaxVolAbove = 60;


// Server.killAll;
s = Server.default;
s.volume.volume = -4; // Maximum volume is 0 dB, negative is quieter

~standby = true;

~start = {
	~dryBus          = Bus.audio(s, 2);
	~mixBus          = Bus.control(s, 1);
	~envDryWetBus    = Bus.control(s, 1);
	~envFxBalanceBus = Bus.control(s, 1);


	0.5.wait;
	// ----------------------------
	"0. Load audio files".postln;

	Buffer.freeAll;
	~buffers = [];
	~currDir = PathName.new(thisProcess.nowExecutingPath).pathOnly;
	if(~currDir.isNil, {
		// Fix because when loaded from startup.scd we get nil
		~currDir = "C:/Users/44p0/Documents/2021_color_to_sound";
	});
	if(~currDir.isNil, {
		~currDir ++ " not found".postln;
	}, {
		"  Reading folder content".postln;
		~currDir = ~currDir ++ "/samples/";
		PathName.new(~currDir).filesDo({ |f|
			if(f.extension.toLower == "wav", {
				("  " ++ f.fullPath).postln;
				~buffers = ~buffers.add(
					Buffer.readChannel(s, f.fullPath, channels: [0])
				);
			});
		});
		if(~buffers.size == 12, {
			"  Nice: 12 audio files loaded.".postln;
		}, {
			("  Error: 12 audio files should be loaded but found " ++
				~buffers.size ++ " instead!"
			).postln;
		})
	});


	0.5.wait;
	// ----------------------------
	"1. Define synthesizers".postln;

	0.5.wait;

	SynthDef(\mix, {
		arg bus;
		var mix = Clip.kr(LFNoise2.kr(0.1, 8), -1, 1);
		mix.scope;
		Out.kr(bus, mix);
	}).add;

	SynthDef(\sy, {
		arg f = 100, a = 0.1, nf = 0.5, filterFreq = 1000, gbuf;

		var lagFreq1 = Lag.kr(f, lagTime: nf * 0.05 + 0.1);
		var lagFreq2 = Lag.kr(f * 0.5, lagTime: nf * 0.03 + 0.15);
		var detune = LFNoise2.kr([nf, nf * 2.0], 0.005, 1.0);
		var f1 = lagFreq1 * detune;
		var f2 = lagFreq2 * detune;
		var amp = Lag.kr(a, nf * 0.03 + 0.02) *
		          LFNoise2.kr(nf * 0.7, mul: 0.05, add: 0.95);
		var w1 = LFTri.ar(f1, nf, amp * 0.85) + WhiteNoise.ar(amp * 0.2);
		var w2 = Pulse.ar(f2, SinOsc.kr(0.03, phase: nf, mul: 0.4, add: 0.5), amp);
		var wave = XFade2.ar(w1, w2, In.kr(~mixBus));

		var grainPos = LFNoise2.kr(freq:~travelSpeed, mul:0.4, add:0.41) +
			LFNoise0.kr(mul:0.005, add:0.005);
		var grain = GrainBuf.ar(
			trigger: Impulse.kr(80), dur: 0.05, sndbuf: gbuf,
			rate: BufRateScale.kr(gbuf), mul: amp * 2, pos: grainPos);

		var snd = XFade2.ar(wave, [grain, grain], ~grainMix);

		var filterReso = SinOsc.kr(0.1 * nf, nf, 0.05, 0.5);
		var filtered = RLPF.ar(snd, Lag.kr(filterFreq), filterReso);
		var lessBass = HPF.ar(filtered, ~removeBassBelowFreq);

		Out.ar(~dryBus, lessBass);
	}).add;

	// This was mouse based before (therefore the "click") but to avoid mistakes I made
	// it automatic (no more mouse interaction)
	SynthDef(\slowClickToStartEnvelope, {
		arg bus, fxEnvSpeed;
		var fxFadeInEnv = EnvGen.kr(
			Env(
				levels: [0, 0, 0, 0.5, 1.0],
				times: [0.05, ~fxInitialSilence, ~fxFadeInDuration/2, ~fxFadeInDuration/2],
				curve: [1, 1, 4, -4]
			)
		);

		// LFNoise1 is signed (-1 ~ +1)
		var fxAmt = Clip.kr(LFNoise2.kr(fxEnvSpeed) * fxFadeInEnv + 0.5);

		Out.kr(bus, fxAmt);
	}).add;

	~fx = SynthDef(\fxSynth, {
		arg outBus = 0, amp = 0.0, ampSlow = 0.0;
		var dryAmt = In.kr(~envDryWetBus);
		var fxBalance = In.kr(~envFxBalanceBus);
		var dry = In.ar(~dryBus, 2);
		var fxA = InsideOut.ar(dry, 0.015 * Amplitude.kr(dry)) + dry;
		var fxB = dry * SinOsc.kr(10, 0, 0.2, 0.8);
		var effected = Limiter.ar((fxBalance * fxB) + ((1-fxBalance) * fxA));
		var wet = ((1-dryAmt) * dry) + (dryAmt * effected);

		dryAmt.scope(zoom: 100);
		fxBalance.scope(zoom: 100);
		Out.ar(outBus, wet * VarLag.kr(amp, time: 0.05) * VarLag.kr(ampSlow, time: 4.00));
	}).play;



	// ----------------------------
	"2. Start synthesizers".postln;
	0.5.wait;
	Synth(\mix, [\bus, ~mixBus]);
	Synth(\slowClickToStartEnvelope, [\bus, ~envDryWetBus, \fxEnvSpeed, 0.25]);
	Synth(\slowClickToStartEnvelope, [\bus, ~envFxBalanceBus, \fxEnvSpeed, 0.15]);

	~partials = Array.fill(12, { arg i; Synth.before(~fx, \sy, [
		\f, 100.rrand(200),
		\nf, 0.5.rrand(3.0),
		\gbuf, ~buffers[i]
	]) });


	0.5.wait;
	// ----------------------------
	"3. Listen to OSC messages".postln;
	OSCdef(\osc_data, { |msg|
		var maxSat = 0, satToVol, tilt;

		12.do { |i|	maxSat = max(maxSat, msg[i*3+2]); };
		satToVol = maxSat.explin(~saturationMutedBelow, ~saturationMaxVolAbove, 0, 1);

		12.do { |i|
			var hue = msg[i*3+1]; //0..360
			var sat = msg[i*3+2]; //0..100
			var bri = msg[i*3+3]; //0..100
			var octave = (i / 3).asInteger;

			//var note = (hue / 16 * 1.5).asInteger;
			//var note = hue / 10; // testing no quantization // 360 / 10 = 36 different
			var note = hue / 30; // 12 different tones
			var frq = Scale.major.degreeToFreq(note, 36.midicps, octave);
			var bufNum = note.asInteger % 12;
			var vol = (1-(((bri/100)-1).abs)) * 0.1 * satToVol;
			var ff = sat * 18 + 200;

			~partials[i].set(
				\f, frq,
				\a, vol,
				\filterFreq, ff,
				\gbuf, ~buffers[bufNum]
			);
		};

		// last value in the message is tablet accelerometer.x
		tilt = msg.last;
		~fx.set(\amp, 1.0 - (tilt / 100.0).abs );

		~fx.set(\ampSlow, if(~standby, { 0.0 }, { 1.0 }));

	}, '/d');

	OSCdef(\standby, { |msg|
		~standby = msg[1];
	}, '/standby');

	0.5.wait;

	// Test random OSC values
	NetAddr("localhost", 57120).sendMsg("/d", * ({ 254.rand }.dup(36) ++ 1.0));
};

s.waitForBoot(~start);
//s.boot;
)










