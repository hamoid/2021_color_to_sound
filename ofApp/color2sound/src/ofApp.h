#include <ofBaseApp.h>
#include <ofVideoPlayer.h>
#include <ofVideoGrabber.h>
#include <ofxOscSender.h>
#include <ofxNetwork.h>
#include <ofShader.h>
#include <ofImage.h>
#include <ofFbo.h>

#pragma once


class ofApp : public ofBaseApp {

    private:
        static const int W { 4 };
        static const int H { 3 };
        static const int margin { 100 };
        static const int videoW { 640 };
        static const int videoH { 480 };

        std::vector<glm::ivec4> coords;
        float hues [W * H];
        int32_t data [W * H * 3];
        int debugLayerH;

        float opacity { 0 };

        bool standby { true };
        bool menuVisible { false };

#ifdef TARGET_LINUX
        bool debug { true };
        ofVideoPlayer cam;
#else
        bool debug { false };
        ofVideoGrabber cam;
#endif

        ofxOscSender supercollider;

        ofShader blurH;
        ofShader blurV;

        ofPixels px;

        ofImage i_sleep, i_wakeup, i_quit;

        ofFbo pass1;
        ofFbo pass2;
        ofFbo debugLayer;

        int tilt { 0 };
        ofxUDPManager udp;

        void populateSamplingCoords();
        void setupDebugLayer();
        void drawMenu();

    public:
        void setup();
        void update();
        void draw();

        void keyPressed(int key);
        void mousePressed(int x, int y, int button);
        //void keyReleased(int key);
        void mouseReleased(int x, int y, int button);
        //void mouseEntered(int x, int y);
        //void mouseExited(int x, int y);
        //void windowResized(int w, int h);
        //void dragEvent(ofDragInfo dragInfo);
        //void gotMessage(ofMessage msg);

};
