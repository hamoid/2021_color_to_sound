#include "ofApp.h"
#include "ofAppRunner.h"
#include "ofGraphics.h"
#include "ofMath.h"

/**
 * TODO
 *
 * - [x] load video for debugging
 * - [x] install qtcreator in tablet
 * - [x] install openframeworks in tablet
 * - [x] try ofGetOrientation() on of/tablet
 * - [x] port shaders
 * - [x] receive UDP messages with x accelerometer value between -100 and +100
 * */

//--------------------------------------------------------------
void ofApp::populateSamplingCoords() {
    int i = 0;
    for(int y = 0; y < H; y++) {
        for(int x = 0; x < W; x++) {
            int xx = floor(ofMap(x, 0, W - 1, margin, ofGetWidth() - margin));
            int yy = floor(ofMap(y, 0, H - 1, margin, ofGetHeight() - margin));
            int xvid = floor(ofMap(x, 0, W - 1, margin, videoW - margin));
            int yvid = floor(ofMap(y, 0, H - 1, margin, videoH - margin));
//            if(!debug) {
//                yy = ofGetHeight() - yy;
//                yvid = videoH - yvid;
//            }
//            int off = (yvid * videoW + xvid) * 4;
            coords.push_back(glm::ivec4(xx, yy, xvid, yvid));
            //std::cout << xx << " " << yy << " " << off << std::endl;
        }
    }
}

//--------------------------------------------------------------
void ofApp::setupDebugLayer() {
    if(debug) {
        debugLayerH = ofGetWindowHeight() / (W * H);
        debugLayer.allocate(ofGetWindowWidth(), ofGetWindowHeight());
        ofLog() << "debugLayer: " << debugLayer.getWidth() << " x " <<
            debugLayer.getHeight();
        debugLayer.begin();
        ofClear(255, 255, 255, 0);
        ofFill();
        ofSetColor(0, 0, 0, 10);
        for(int i=0; i<W*H; i+=2) {
            ofDrawRectangle(0, debugLayerH * i, debugLayer.getWidth(), debugLayerH);
        }
        debugLayer.end();
    }
}

//--------------------------------------------------------------
void ofApp::setup() {
    ofDisableArbTex();
    ofSetWindowTitle("color2sound");
    ofSetVerticalSync(true);
    ofSetFrameRate(30);
    ofSetBackgroundColor(0);
    supercollider.setup("127.0.0.1", 57120);

    #ifdef TARGET_LINUX
        cam.load("atelier2b.mp4");
        cam.setLoopState(OF_LOOP_NORMAL);
    #else
        auto devices = cam.listDevices();
        for (auto & dev : devices) {
            ofLogNotice() << dev.id << ": " << dev.deviceName;
            if(!dev.bAvailable) { ofLogNotice() << " ^ unavailable"; }
        }

        if(devices.size() > 0) {
            cam.setDeviceID(devices.size() - 1);
            cam.setDesiredFrameRate(30);
            cam.setup(640, 480);
        }
        ofHideCursor();
    #endif

    //UDP for accelerometer data
    ofxUDPSettings udpSettings;
    udpSettings.receiveOn(15000);
    udpSettings.blocking = false;
    udp.Setup(udpSettings);

    blurH.load("shadersGL3/shaderBlurX");
    blurV.load("shadersGL3/shaderBlurY");

    pass1.allocate(videoW, videoH);
    pass2.allocate(videoW, videoH);

    setupDebugLayer();
    populateSamplingCoords();

    i_sleep.load("sleep.png");
    i_wakeup.load("wakeup.png");
    i_quit.load("quit.png");
}

//--------------------------------------------------------------
void ofApp::update() {
    cam.update();

    // UDP message
    char msg[1];
    int numBytes = 0;
    do {
        numBytes = udp.Receive(msg, 1);
        if(numBytes > 0) {
            tilt = static_cast<int>(msg[0]);
            //ofLog() << tilt;
        }
    } while(numBytes > 0);
}

//--------------------------------------------------------------
void ofApp::drawMenu() {
    if(ofGetMousePressed()) {
        if(standby) {
            i_wakeup.draw(ofGetWidth() / 2 - i_wakeup.getWidth() / 2, i_wakeup.getHeight() * 0.5);
        } else {
            i_sleep.draw(ofGetWidth() / 2 - i_sleep.getWidth() / 2, i_sleep.getWidth() * 0.5);
        }
        i_quit.draw(ofGetWidth() / 2 - i_quit.getWidth() / 2, ofGetHeight() - i_quit.getHeight() * 1.5);
    }
}

//--------------------------------------------------------------
void ofApp::draw() {
    ofClear(0);
    ofSetColor(ofColor::white);

    opacity = ofClamp(opacity + (standby ? -2 : 2), 0, 255);
    if(opacity < 1) {
        drawMenu();
        return;
    }

    pass1.begin();
    blurH.begin();
    blurH.setUniform1f("blurAmnt", 4);
    blurH.setUniform1f("texwidth", cam.getWidth());
    cam.draw(0, 0);
    blurH.end();
    pass1.end();

    pass2.begin();
    blurV.begin();
    blurV.setUniform1f("blurAmnt", 4);
    blurV.setUniform1f("texheight", cam.getHeight());
    pass1.draw(0, 0);
    blurV.end();
    pass2.end();

    // Draw non-blurry cam
    ofSetColor(opacity);
    cam.draw(0, 0, ofGetWidth(), ofGetHeight());
    ofSetColor(255);

    drawMenu();

    pass2.readToPixels(px);
    px.mirror(true, false);

    auto maxSat = 0;
    auto fence = 5;

    for(int i = 0; i < coords.size(); i++) {
        auto & coord = coords[i];

        //auto xx = coord.x;
        //auto yy = coord.y;
        auto xv = coord.z;
        auto yv = coord.w;

        auto c = px.getColor(xv, yv);
        auto cc = ofColor(c, opacity);

        ofSetColor(cc);
        ofDrawRectangle(coord.x - 20, ofGetHeight() - coord.y - 20, 40, 40);

        auto newHue = cc.getHueAngle();
        auto newBri = cc.getBrightness();
        auto newSat = cc.getSaturation();

        auto oldHue = data[i*3+0];
        auto quaHue = floor(hues[i] / 18) * 18;

        // if the new hue goes far enough into the previous or next bucket,
        // update the bucket's hue. the purpose of this is to avoid updating
        // the hue if it is oscillating around the border between two buckets,
        // otherwise the sonified pitch would jump constantly. I want it to
        // change only if the hue has obviously changed.
        // A possible gotcha with this may be that hues of dark colors can be

        // unstable (rgb noise).
        if(oldHue != quaHue) {
            if(abs(newHue - oldHue) > 180) {
                if(newHue < oldHue) {
                    newHue += 360;
                } else {
                    oldHue += 360;
                }
            }

            if(newHue < oldHue - fence || newHue >= oldHue + 18 + fence) {
                data[i*3+0] = quaHue;
            }
        }
        data[i*3+1] = newSat;
        data[i*3+2] = newBri;

        hues[i] = newHue;
    }

    #ifdef TARGET_LINUX
        auto xPos = ofGetWidth() * cam.getPosition();

        debugLayer.begin();
        ofSetColor(255, 0, 0);
        for(int i=0; i<12; i++) {
            ofDrawRectangle(xPos, ofGetWindowHeight() - debugLayerH * (5 + (data[i*3+0] / 360.0)), 1, 1);
        }
        ofSetColor(0, 255, 0);
        for(int i=0; i<12; i++) {
            ofDrawRectangle(xPos, ofGetWindowHeight() - debugLayerH * (5 + (data[i*3+1] / 255.0)), 1, 1);
        }
        ofSetColor(0, 0, 255);
        for(int i=0; i<12; i++) {
            ofDrawRectangle(xPos, ofGetWindowHeight() - debugLayerH * (5 + (data[i*3+2] / 255.0)), 1, 1);
        }
        debugLayer.end();

        ofSetColor(ofColor::white);
        debugLayer.draw(0, 0);

        // video progress indicator
        ofSetColor(255, 50);
        ofDrawLine(xPos, 0, xPos, ofGetHeight());

        // FPS
        ofSetColor(0, 255, 255);
        ofDrawRectangle(0, 5,
            ofGetWidth() * ofGetFrameRate() / ofGetTargetFrameRate(), 2);
    #endif

    // send message containing color analysis and tilt
    ofxOscMessage msg;
    msg.setAddress("/d");
    for(int i = 0; i < W * H * 3; i++) {
        msg.addIntArg(data[i]);
    }
    msg.addIntArg(tilt);
    supercollider.sendMessage(msg);

    // send standby status once per second
    if(ofGetFrameNum() % 30 == 15) {
        ofxOscMessage msg;
        msg.setAddress("/standby");
        msg.addBoolArg(standby);
        supercollider.sendMessage(msg);
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) {
    #ifdef TARGET_LINUX
        if(key == ' ') {
            cam.setPaused(!cam.isPaused());
        }
        if(key == OF_KEY_RETURN) {
            cam.setPosition(ofGetMouseX() * 1.0f / ofGetWindowWidth());
        }
    #endif
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {
    if(y > ofGetHeight() * 0.3 && y < ofGetHeight() * 0.7) {
        menuVisible = true;
    }
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {
    if(!menuVisible) {
        return;
    }
    if(y < ofGetHeight() * 0.3) {
        standby = !standby;
    }

    if(y > ofGetHeight() * 0.7) {
        ofExit(0);
    }
}
