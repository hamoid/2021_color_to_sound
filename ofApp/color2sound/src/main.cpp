#include <iostream>

#include "ofApp.h"
#include "ofAppRunner.h"
#include "ofWindowSettings.h"

int main( ) {
    //ofSetLogLevel(OF_LOG_VERBOSE);
    int windowWidth = 640;
    int windowHeight = 480;

    ofGLWindowSettings settings;
    settings.setSize(windowWidth, windowHeight);
    settings.setGLVersion(3, 2);
    settings.windowMode = OF_FULLSCREEN; // OF_WINDOW or OF_FULLSCREEN

    auto window = ofCreateWindow(settings);
    ofRunApp(window, std::make_shared<ofApp>());

    ofRunMainLoop();
}
