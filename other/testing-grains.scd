"START".postln;

(
q = ();

Buffer.freeAll;

q.currPath = Document.current.dir;
if(q.currPath.isNil, {
	"Sample folder not found".postln;
}, {
	"Reading folder content".postln;
	q.currPath = q.currPath ++ "/samples/";
    PathName.new(q.currPath).files.do({ |name, i|
        name.fullPath.postln;
        q.buffers = q.buffers.add( Buffer.readChannel(s, name.fullPath, channels: [0]) );
    });
	("" ++ q.buffers.size ++ " loaded samples.").postln;
});
q.noise = Bus.control(s, 1);
)

(
fork {
	1.wait; // ------------------------- ---------------------------

	SynthDef(\grain, {
		arg bufNum, dur = 0.1, amp = 0.5;

		var env = EnvGen.kr(
			Env.new([0, 1, 0], [dur * 0.1, dur]),
			doneAction: Done.freeSelf);

		var grain = PlayBuf.ar(2, bufNum,
			rate: BufRateScale.kr(bufNum),
			trigger: 1,
			startPos: BufFrames.ir(bufNum) * In.kr(q.noise));

		var snd = Compander.ar(grain, grain,
			thresh: 0.2,
			slopeBelow: 0.1,
			slopeAbove: 1,
			clampTime: 0.005,
			relaxTime: 0.01);

		Out.ar(0, snd * env * amp);
	}).add;

	SynthDef(\noise, {
		Out.kr(q.noise,
			LFNoise2.kr(freq:0.2, mul:0.4, add:0.4) +
			LFNoise0.kr(mul:0.005, add:0.005)
		);
	}).add;

}
)

(
fork {
	Synth(\noise);
	1.wait;
	while(
		{ true }, {
			Synth.grain(\grain, [ \bufNum, q.buffers[11].bufnum ]);
			0.01.wait;
		}
	)
}
)

// ---- different approach

(
SynthDef(\noise2, {
	Out.kr(q.noise,
		LFNoise2.kr(freq:0.4, mul:0.4, add:0.4) +
		LFNoise0.kr(mul:0.005, add:0.005)
	);
}).add;

SynthDef(\grest, {
	arg out, buf = 0; // gate = 1,
	// var env = EnvGen.kr(
	// 	envelope: Env([0, 1, 0], [1, 1], \sin, 1),
	// 	gate: gate,
	// 	levelScale: 1,
	// doneAction: Done.freeSelf);
	var snd = GrainBuf.ar(
		trigger: Impulse.kr(50),
		dur: 0.1,
		sndbuf: buf,
		rate: BufRateScale.kr(buf),
		pos: In.kr(q.noise)); // * env
	Out.ar(out, [snd, snd]);
}).add;
)

Synth(\noise2);
x = Synth(\grest, [\buf, q.buffers[11]])
x.set(\buf, q.buffers[11])
x.set(\gate, 0);
