const url = "wss://localhost:8443";

// sample n buckets.
// Each gives hue.
// Quantize hue.
// Saturation controls filter and vol.
// Black and white should not produce sound, but gray yes.
// volume = 1-(distance to 0.5 bri)
// to avoid jitter, implement tolerance to swap bucket.

// See https://preview.p5js.org/AndWeiss/present/yBiR2O09q

let blurH, blurV;
let vid;
let coords = [];
let pass1, pass2;
let W = 4, H = 3;
let opacity = 0;
let standby = true;

let hues = new Array(W*H);
hues.fill(0);

// The last item is the accelerometer x value
let data = new Array(W*H*3+1);
data.fill(0);

let active = true;
let debugLayer;
let debugLayerH;
let debug = window.location.href.indexOf('debug') >= 0

const margin = 100;
const ws = new WebSocket(url);
const videoW = 640;
const videoH = 480;

ws.onopen  = ()   => { console.log(`open:  ${url}`); }
ws.onerror = (ev) => { console.log(`err: ${url}`); }
ws.onclose = (ev) => { console.log(`close: ${url} ${ev.code}`); }

function preload() {
  blurH = loadShader('webcam.vert', 'webcam.frag');
  blurV = loadShader('webcam.vert', 'webcam.frag');
}

function setup() {
  pixelDensity(1);
  createCanvas(windowWidth, windowHeight, WEBGL);
  frameRate(30);
  noStroke();
  noCursor();
  rectMode(CENTER);
  background(0);

  // Set up `vid`
  if(debug) {
    vid = createVideo(['atelier2c.mp4']);
  } else {
    vid = createCapture({
      audio: false,
      video: {
        facingMode: { exact: "environment" },
        width: videoW,
        height: videoH
      }
    });
  }
  vid.hide();

  // Set up video Graphics for applying blur shader
  pass1 = createGraphics(videoW, videoH, WEBGL);
  pass2 = createGraphics(videoW, videoH, WEBGL);
  pass1.noStroke();
  pass2.noStroke();

  setupDebugLayer();
  populateSamplingCoords();
}

// If the window is resized
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
  setupDebugLayer();
  populateSamplingCoords();
  background(0);
}

function setupDebugLayer() {
  console.log("setupDebugLayer", debug);
  if(debug) {
    debugLayerH = windowHeight / (W*H);
    debugLayer = createGraphics(windowWidth, windowHeight);
    console.log("createGraphics", windowWidth, windowHeight);
    debugLayer.clear();
    debugLayer.noStroke();
    debugLayer.fill(0, 10);
    for(let i=0; i<W*H; i+=2) {
      debugLayer.rect(0, debugLayerH * i, windowWidth, debugLayerH);
    }
  }
}

// Calculate color sampling screen locations
function populateSamplingCoords() {
  console.log("populateSamplingCoords", windowWidth, windowHeight);
  coords.length = 0;
  for(let y=0; y<H; y++) {
    for(let x=0; x<W; x++) {
      let xx = Math.floor(map(x, 0, W-1, margin, windowWidth-margin));
      let yy = Math.floor(map(y, 0, H-1, margin, windowHeight-margin));
      let xvid = Math.floor(map(x, 0, W-1, margin, videoW-margin));
      let yvid = Math.floor(map(y, 0, H-1, margin, videoH-margin));
      if(!debug) {
        yy = windowHeight - yy;
        yvid = videoH - yvid;
      }
      let off = (yvid * videoW + xvid) * 4;
      coords.push([xx, yy, off]);
    }
  }
}


// a bucket has h, s, b, quant_h. if h moves far enough from quant_h, then
// quant_h changes.
function draw() {
  opacity = constrain(opacity + (standby ? -2 : 2), 0, 255);
  if(!active || opacity < 1) {
    return;
  }
  background(0);

  pass1.shader(blurH);
  blurH.setUniform('tex0', vid);
  blurH.setUniform('texelSize', [1.0/videoW, 1.0/videoH]);
  blurH.setUniform('direction', [1.0, 0.0]);
  pass1.rect(0, 0, videoW, videoH);

  pass2.shader(blurV);
  blurV.setUniform('tex0', pass1);
  blurV.setUniform('texelSize', [1.0/videoW, 1.0/videoH]);
  blurV.setUniform('direction', [0.0, 1.0]);
  pass2.rect(0, 0, videoW, videoH);
  pass2.loadPixels();

  translate(-windowWidth/2, -windowHeight/2);
  tint(255, opacity);
  image(vid, 0, 0, windowWidth, windowHeight);
  tint(255);

  let h, s, b, c;
  let quaHue, oldHue;
  let newHue, newSat, newBri;
  let maxSat = 0;
  let px = pass2.pixels;
  let fence = 5;
  let xPos = windowWidth * vid.time() / vid.duration();

  coords.forEach((p, i) => {
    let [xx, yy, off] = p;
    c = color(
      px[off],
      px[off+1],
      px[off+2], opacity
    );
    fill(c);
    rect(xx, windowHeight-yy, 40, 40);

    newHue = hue(c);
    newBri = Math.floor(brightness(c));
    newSat = Math.floor(saturation(c));

    maxSat = Math.max(maxSat, newSat);
    oldHue = data[i*3+0];
    quaHue = Math.floor(hues[i] / 18) * 18;
    // 360 / 18 = 20 (possible hue buckets)

    // if the new hue goes far enough into the previous or next bucket,
    // update the bucket's hue. the purpose of this is to avoid updating
    // the hue if it is oscillating around the border between two buckets,
    // otherwise the sonified pitch would jump constantly. I want it to
    // change only if the hue has obviously changed.
    // A possible gotcha with this may be that hues of dark colors can be
    // unstable (rgb noise).
    if(oldHue != quaHue) {
      if(abs(newHue - oldHue) > 180) {
        if(newHue < oldHue) {
          newHue += 360;
        } else {
          oldHue += 360;
        }
      }

      if(newHue < oldHue - fence || newHue >= oldHue + 18 + fence) {
        data[i*3+0] = quaHue;
      }
    }
    data[i*3+1] = newSat;
    data[i*3+2] = newBri;

    hues[i] = newHue;
  });

  // 1.0 on landscape, 0.0 on portrait
  let horizontality = Math.max(0.0, Math.min(1.0, 1.0 - Math.abs(window.accX / 9.81)));
  // 1.0 on full opacity, 0.0 when faded out
  let opacityNorm = opacity / 255;

  data[data.length - 1] = horizontality * opacityNorm;

  if(debug) {
    debugLayer.stroke(255, 0, 0); // red = hue
    for(let i=0; i<12; i++) {
      debugLayer.point(xPos, windowHeight - debugLayerH * (5 + (data[i*3+0] / 360.0)));
    }
    debugLayer.stroke(0, 255, 0); // green = sat
    for(let i=0; i<12; i++) {
      debugLayer.point(xPos, windowHeight - debugLayerH * (5 + (data[i*3+1] / 100.0)));
    }
    debugLayer.stroke(0, 0, 255); // blue = bri
    for(let i=0; i<12; i++) {
      debugLayer.point(xPos, windowHeight - debugLayerH * (5 + (data[i*3+2] / 100.0)));
    }
    image(debugLayer, 0, 0);

    stroke(255, 50);
    line(xPos, 0, xPos, windowHeight);
    noStroke();

    if(frameCount % 10 == 0) {
      print(maxSat);
    }

    // FPS
    fill(0,255,255);
    rect(0, 5, windowWidth * frameRate() / 30.0, 2);
  }

  ws.send(JSON.stringify(data));
}

function keyPressed() {
  if(key == ' ') {
    active = !active;
  }
  if(key == 'd') {
    debug = !debug;
  }
  if(debug) {
    if(keyCode == ESCAPE) {
      vid.pause();
    }
    if(keyCode == ENTER) {
      let t = Math.floor(vid.duration() * mouseX / windowWidth);
      vid.time(t);
      vid.loop();
    }
  }
}

function mousePressed() {
  if(mouseY < 100) {
    fullscreen(!fullscreen());
  } else {
    standby = !standby;
  }
}


