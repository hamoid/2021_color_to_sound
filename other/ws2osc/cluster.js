"use strict";

// get euclidean distance between two equal-dimension vectors
const euclideanDistance = (a, b) => {
  const size = Math.min(a.length, b.length);
  let sum = 0;

  for (let index = 0; index < size; index++)
    sum += (a[index] - b[index]) * (a[index] - b[index]);

  return Math.sqrt(sum);
};

// get average distance between sets of indexes, given distance matrix
const averageDistance = (setA, setB, distances) => {
  let distance = 0;

  for (const a of setA) {
    for (const b of setB) distance += distances[a][b];
  }

  return distance / setA.length / setB.length;
};

// the main clustering function
const clusterData = ({
  data = [],
  key = '',
  distance = euclideanDistance,
  linkage = averageDistance
}) => {
  // extract values from specified key
  if (key) data = data.map(datum => datum[key]);

  // compute distance between each data point and every other data point
  // N x N matrix where N = data.length
  const distances = data.map((datum, index) => data.map(otherDatum => distance(datum, otherDatum)));

  // initialize clusters to match data
  const clusters = data.map((datum, index) => ({
    height: 0,
    indexes: [Number(index)]
  }));

  // keep track of all tree slices
  let clustersGivenK = [];

  // iterate through data
  for (let iteration = 0; iteration < data.length; iteration++) {

    // add current tree slice$
    clustersGivenK.push(clusters.map(cluster => cluster.indexes));

    // dont find clusters to merge when only one cluster left
    if (iteration >= data.length - 1) break;

    // initialize smallest distance
    let nearestDistance = Infinity;
    let nearestRow = 0;
    let nearestCol = 0;

    // upper triangular matrix of clusters
    for (let row = 0; row < clusters.length; row++) {
      for (let col = row + 1; col < clusters.length; col++) {
        // calculate distance between clusters
        const distance = linkage(clusters[row].indexes, clusters[col].indexes, distances);

        // update smallest distance
        if (distance < nearestDistance) {
          nearestDistance = distance;
          nearestRow = row;
          nearestCol = col;
        }
      }
    }

    // merge nearestRow and nearestCol clusters together
    const newCluster = {
      indexes: [...clusters[nearestRow].indexes, ...clusters[nearestCol].indexes],
      height: nearestDistance,
      children: [clusters[nearestRow], clusters[nearestCol]]
    };

    // remove nearestRow and nearestCol clusters
    // splice higher index first so it doesn't affect second splice
    clusters.splice(Math.max(nearestRow, nearestCol), 1);
    clusters.splice(Math.min(nearestRow, nearestCol), 1);

    // add new merged cluster
    clusters.push(newCluster);
  }

  // assemble full list of tree slices into array where index = k
  clustersGivenK = [[], ...clustersGivenK.reverse()];

  return {
    clusters: clusters[0],
    distances: distances,
    order: clusters[0].indexes,
    clustersGivenK: clustersGivenK
  };
};

// https://github.com/greenelab/hclust#comparison-with-hclusterjs

const palette = [
  [1,2,1],
  [1,3,1],
  [1,4,1],
  [2,2,1],
  [2,2,2],

  [5,6,5],
  [6,6,6],
  [5,5,5],
  [6,5,5],

  [9,1,1],

  [9,1,9],
  [9,5,9]
];

const t0 = process.hrtime();
const result = clusterData({ data: palette });

const threshold = 2;

function explore(data) {
  if(data.height < threshold) {
    let sum = [0, 0, 0];
    for(let i of data.indexes) {
      const rgb = palette[i];
      sum[0] += rgb[0];
      sum[1] += rgb[1];
      sum[2] += rgb[2];
    }
    let L = data.indexes.length;
    //console.log(data.indexes);
    //console.log(sum[0]/L, sum[1]/L, sum[2]/L);
  } else {
    let c = data.children;
    if(c[0].height > 0) {
      explore(c[0])
    } else {
      //console.log(c[0]);
    }
    if(c[1].height > 0) {
      explore(c[1])
    } else {
      //console.log(c[1]);
    }
  }
}

explore(result.clusters);

const t1 = process.hrtime(t0);
console.info('%ds %dms', t1[0], t1[1] / 1000000)
