import { WebSocketServer } from 'ws';
import { Bundle, Client } from 'node-osc';
import http from 'http';
import https from 'https';
import fs from 'fs';
import os from 'os';

// OSC - https://github.com/MylesBorins/node-osc
const osc = new Client('127.0.0.1', 57120); // 12000

// HTTPS
const options = {
  key: fs.readFileSync('certs/key.pem'),
  cert: fs.readFileSync('certs/cert.pem')
};
const www = https.createServer(options, (req, res) => {
  let f = req.url.replace('/', '') || 'index.html';
  console.log(`www: ${f}`);
  if(f.endsWith(".mp4")) {
    res.setHeader("Content-Type", "video/mp4");
    fs.readFile(f, function (err, data) {
      res.writeHead(200);
      res.end(data);
    });
    return;
  }
  fs.readFile(f, 'utf8', function (err, data) {
    if(f.endsWith(".js")) {
      res.setHeader("Content-Type", "application/javascript");
    }
    res.writeHead(200);
    res.end(data);
  });
}).listen(8443);

// WebSockets
const wss = new WebSocketServer({ server: www });
wss.on("connection", (ws) => {
  console.log("client connected");
  ws.on("message", (msg) => {
    osc.send('/d', JSON.parse(msg));
  });
});

const interval = setInterval(() => {
  wss.clients.forEach((ws) => { ws.ping(); });
}, 5000);

wss.on('close', () => {
  clearInterval(interval);
  console.log("wss closed");
});

// Info
console.log(`osc: ${osc.host}:${osc.port}`);
console.log('Do not close this window until performance is finished.');

