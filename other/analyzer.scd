// This program follows the pitch of an input sound
// and resynthesizes it using a synth, reverb, delay and
// distortion

( // Boot
//s.options.memSize_(65536 * 2);
//s.boot;
//Quarks.gui;
)

/*
generate chords using multiple colors.
use p5.js to look at webcam and send values to SC.
slow fade in and out.
planned shows starting in January.
*/

( // Clean
Ndef(\src).clear;
Ndef(\reverb).clear;
Ndef(\delay).clear;
)

( // UI
Spec.add(\duration, [0.1, 60, \exp]);
Spec.add(\damp, [0, 1]);
Spec.add(\size, [0.5, 3]);
Spec.add(\earlydiff, [0, 1]);
Spec.add(\mdepth, [0, 50]);
Spec.add(\mfreq, [0, 10]);
Spec.add(\filterFreq, [1000, 10000, \exp]);
Spec.add(\filterReso, [0.1, 1.0]);
)

( // Audio input
// Recording for testing
r = Buffer.readChannel(s, "/home/funpro/Desktop/work/2021_color_to_sound/analyze.wav", channels: [0]);
// Live
//r = SoundIn.ar(0)
)

(
// # Synth
var scale = Scale.choose;
scale.postln;
Ndef(\src, {
    arg bufnum;
	var sound, amp, chain, scalebuffer,
	frq, trigger, ampLFO, detuner, adsr, env,
	filterFreq, filterReso, tone;
	var envTime = 0.001;

	sound = PlayBuf.ar(1, r, rate: 1.0, loop: 1.0);
	amp = Amplitude.kr(sound, 0.1, 0.1);
	chain = FFT(LocalBuf(2048, 1), sound);

	scalebuffer = Buffer.alloc(s, scale.size, 1, {|b| b.setnMsg(0, scale) });

	frq = SpecCentroid.kr(chain);

	frq = DegreeToKey.kr(
		scalebuffer.bufnum,
		frq.cpsmidi.round,
		scale.stepsPerOctave, 0.2, 30
	).midicps.min(10000);

	trigger = Changed.kr(frq, 0.1);
	ampLFO = SinOsc.kr(10, mul: 0.05, add: 0.4);
	detuner = LFNoise1.kr([3, 4], 0.1);
	adsr = Env(
		levels: [0, 1, 1, 0],
		times: [0.01, 0.05, 1.0],
		curve: [\linear, \hold, \squared]
	);
	env = EnvGen.kr(adsr, trigger);
	filterFreq = 50.0 + (env * \filterFreq.kr(1000, 0.05));
	filterReso = \filterReso.kr(0.1, 0.05);

	tone = (LFSaw.ar(frq + detuner) * ampLFO * 0.75) +
	(SinOsc.ar([frq * 0.5, frq * 0.25]) * 0.25);

	// Show frq once per second
	//Poll.kr(Impulse.kr(1), frq, "frq");

	// Distortion: TODO: make it configurable
    tone = DelayC.ar(tone, 0.01, envTime * 0.5) *
	 EnvFollow.ar(tone, 1 - (envTime * SampleRate.ir).reciprocal).max(-80.0.dbamp).reciprocal * 0.3;


	RLPF.ar(tone * amp * env, filterFreq, filterReso); // + (sound * 0.1);
});
Ndef(\src).fadeTime = 0.5;
)

(
// # Reverb
Ndef(\reverb, {
    var src = Ndef(\src).ar * \amp.kr(1);
    src = JPverb.ar(
        src,
        \duration.kr(1,      0.05),
        \damp.kr(0,          0.05),
        \size.kr(1,          0.05),
        \earlydiff.kr(0.707, 0.05),
        \mdepth.kr(5,        0.05),
        \mfreq.kr(2,         0.05),
		low: 1.0,
		mid: 1.0,
		high: 1.0,
        lowcut: 500,
		highcut: 200
    );
});
Ndef(\reverb).fadeTime = 0.2;
)

//NdefMixer(s);

(
// # Delay
Ndef(\delay, {
	var src = Ndef(\reverb).ar;
	src =  CombN.ar(src, 1.00, 0.32, 1.0, 0.9, src);
});
Ndef(\delay).fadeTime = 1.0;
)

(
// # Start synths
Ndef(\reverb).play;
Ndef(\src).play;
Ndef(\delay).play;
)

(
// # Show UI
Ndef(\reverb).edit;
Ndef(\src).edit;
Ndef(\delay).edit;
)

(
// # OSC
OSCdef(\osc_t60, {
	arg msg;
	Ndef(\reverb).setn(\t60, msg[1])
}, '/t60');
OSCdef(\osc_filterFreq, {
	arg msg;
	Ndef(\src).setn(\filterFreq, msg[1])
}, '/filterFreq');
OSCdef(\osc_filterReso, {
	arg msg;
	Ndef(\src).setn(\filterReso, msg[1])
}, '/filterReso');
)

(
// # OSC Tests
n = NetAddr("localhost", 57120);
n.sendMsg("/t60", 0.8.rand);
n.sendMsg("/filterFreq", 1000.rrand(10000));
n.sendMsg("/filterReso", 0.1.rrand(1.0));
//OSCdef.all;
//OSCdef.freeAll;
)

