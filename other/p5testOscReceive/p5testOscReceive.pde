import oscP5.*;
import netP5.*;
  
OscP5 oscP5;

int n = 4 * 3 * 3;
int[] hues = new int[n];

void setup() {
  size(360, 360);
  noStroke();
  oscP5 = new OscP5(this, 57120); 
}

void draw() {
  for(int i=0; i<n; i++) {
    fill(hues[i]);
    rect(i * width / n, 0, width / n, height);
  }  
}

void oscEvent(OscMessage msg) {
  for(int i=0; i<n; i++) {
    hues[i] = msg.get(i).intValue();
  }
}
