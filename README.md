# Live video feed sonification project 

*By Abe Pazos for Johan Lorbeer*

Developed late 2021 - early 2022. Handed in to Johan on January 25th, 2022.

Tested on a Macbook laptop + iPad + Bluetooth speaker.

## Version 2

Macbook + iPad replaced by a Surface Go 3 Windows tablet for a less complicated
setup. Handed in to Johan on July 7th, 2022.

## Version 2.1

In November 2022 I tried to port the analisis part to openFrameworks (c++).

This would reduce the project to two parts: analysis (openFrameworks)
and sound engine (SuperCollider).

So far the analysis part has been a web browser running p5.js, communicating
with a local web server via websockets. The web server is written in node.js,
and it sends commands to SuperCollider via OSC.

The only aspect I couldn't quickly figure out was reading the orientation
sensor in C++. There are example is the Microsoft website, but it would
take me more time than I have.

So I'm abandoning for now the openFrameworks approach. I haven't tested the
shaders which I copied from p5.js. There is a file called port.js where
I was commenting out the JS parts which I already ported to C++.

Maybe in the future.

## Version 2.2

June / July 2023

I couldn't read the accelerometer from openFrameworks so I created a simple C#
program that does that and forwards the value to openFrameworks.
I ported the shaders and all the JS code to C++.
Current solution includes

- C++ webcam to OSC app 
- C# sensor to UDP message app 
- SuperCollider for the sound

## Usage

Make sure the Bluetooth speaker is paired with the tablet.

Tap on the Color2Sound icon on the Desktop of the tablet to start.

The screen eventually turns black. Now there are two gestures available:

- Drag up from the center of the screen to unmute / mute.
- Drag down from the center of the screen to close the programs.

Keep the tablet charging while not performing to make sure the battery is
charged.

## Note about sound

The audio software implements granular sound but it is currently set to mute
in multitone.scd at `~grainMix = -1.0;`. One can choose a value between
-1.0 (granular synth muted) to 1.0 (only granular synth).

12 audio samples can be placed inside the samples/ folder. Use 44100 sample
rate, ~5 to ~10 seconds long, 2 channels, 32-bit floating point PCM.
Other wav formats may work, but I think it's better if they are all in the
same format to avoid surprises.

## Description

As can be read above, this project has had various iterations: from iPad +
Macbook to Windows tablet and various approaches to the software.

The current approach runs 3 programs:

SuperCollider: produces sound and it is controlled remotely via OSC messages.

SensorUWPJune2023: custom program written in C# inside Visual Studio which
reads the tablet accelerometer values and sends them to the color2sound app.

color2sound: openFrameworks c++ app which grabs live webcam input, applies
a GLSL shader to blur the image, samples 12 points in that image, sends
hue, saturation and brightness as OSC values to SuperCollider. Also receives
UDP messages from SensorUWPJune2023 and sends the tablet tilt value to
SuperCollider to control the overal volume. Horizontal = full volume,
90 degree rotation is muted. 

## Update and build

To update the project I open the Github application and pull the latest changes
which I did on my own computer.

If only multitone.scd change, no other steps are necessary.

If the openFrameworks app changed, open MSYS2 MINGW64, then:

    cd /c/Users/44p0/Dokumente/2023_color_to_sound/ofApp/color2sound/
    make Release

I don't currently have the SensorUWPJune2023 code in git. To build it,
Opened Visual Studio, right click on the
Projectmappen-Explorer on SensorUWPJune2023 (Universal Windows) and chose
Veröffentlichen > App-Pakete erstellen...

I don't know if after doing that the UWP app ID hashes change. In that case
one may need to update the runWindows.bat script. That's really not intuitive
and there are 5 links in other/todo.txt explaining the steps. It involves
running a bunch of commands on the Windows Powershell.

I have made a backup of the whole system and placed in on a 500Gb disk in
case something happens to the tablet:

128Gb "Surface Go 3" with 8Gb RAM.
Intel Pentium GOLD 6500Y 1.10Ghz 1.61Ghz
Geräte ID 253D3E45-2C3D-43F4-827F-311CBE138F01
Product ID 00356-05912-86013-AAOEM
64-Bit OS

